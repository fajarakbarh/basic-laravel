@extends('layout')

@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<br />
		<h3 align="center">Tambah Data</h3>
		<br />
		@if(count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if(\Session::has('success'))
			<div class="alert alert-success">
				<p>{{\session::get('success')}}</p>
			</div>
		@endif
		<label>Nama</label>
		<form method="post" action="{{url('student')}}">
			{{csrf_field()}}
			<div class="form-group">
				<input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Anda">
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary">
			</div>
		</form>
	</div>
</div>
</div>
@endsection



