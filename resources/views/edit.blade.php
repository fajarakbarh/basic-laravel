@extends('layout')

@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<br />
		<h3 align="center">Edit Data</h3>
		<br />
		@if(count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<label>Nama</label>
		<form method="post" action="{{url(action('StudController@update',$students->id))}}">
			{{csrf_field()}}
			<input type="hidden" name="_method" value="PATCH">
			<div class="form-group">
				<input type="text" name="nama" value="{{$students->nama}}" class="form-control" placeholder="Masukkan Nama Anda">
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="Update">
			</div>
		</form>
	</div>
</div>
</div>
@endsection