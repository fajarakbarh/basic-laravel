@extends('layout')

@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<br />
		<h3 align="center">Daftar Nama</h3>
		@if($message = Session::get('success'))
			<div class="alert alert-success">
				<p>{{$message}}</p>
			</div>
		@endif
		<br />
		<div align="right">
			<a href="{{route('student.create')}}" class="btn btn-primary">Tambah Data</a>
		</div>
		<br>
		<table class="table table-striped table-bordered">
			<tr>
				<th >Nama</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			@foreach($student as $row)
			<tr>
				<td>{{$row['nama']}}</td>
				<td><a class="btn btn-info" href="{{action('StudController@edit',$row['id'])}}">Edit</a></td>
				<td>
					<form method="post" class="delete_form" action="{{action('StudController@destroy',$row['id'])}}">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-danger">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete_form').on('submit',function(){
			if(confirm("Anda yakin ingin menghapus data ini?")){
			return true;
		}
		else{
			return false;
		}
		});
	});
</script>
@endsection


