<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustController extends Controller
{
    public function MyList()
    {
    	$customer =[
		'Fajar',
		'Akbar',
		'Hariansyah',
	];

    return view('internal.customer',[
    	'name' => $customer,
    ]);
    }
}
